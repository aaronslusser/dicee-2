//
//  Dicee_SwiftUIApp.swift
//  Dicee-SwiftUI
//
//  Created by  Amber  on 10/20/21.
//

import SwiftUI

@main
struct Dicee_SwiftUIApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
